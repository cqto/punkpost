fennel = require("fennel")

-- Pass lua traceback to fennel
debug.traceback = fennel.traceback

-- Support loading fennel modules with "require"
table.insert(package.loaders, function(filename)
   if love.filesystem.getInfo(filename) then
      return function(...)
         return fennel.eval(love.filesystem.read(filename), {env=_G, filename=filename}, ...), filename
      end
   end
end)

-- Jump into fennel
require("main.fnl")
