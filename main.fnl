; Push events to REPL
(fn love.load []
  (: (love.thread.newThread "require('love.event')
while 1 do love.event.push('stdin', io.read('*line')) end") :start))

; REPL callback in console
(fn love.handlers.stdin [line]
  (let [(ok val) (pcall fennel.eval line)]
    (print (if ok (fennel.view val) val))))

; Global draw
(fn love.draw []
  (love.graphics.print "Hello from Fennel!\nPress any key to quit" 10 10))

(fn love.keypressed [key]
  (love.event.quit))
